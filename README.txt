/*************************************/
 YouTube Thumbnail With Time Formatter
/************************************/

Author: Kayode Odeyemi (drecute)

Requires: video_cck zend emthumb 

Description
-----------
YouTube Thumbnail With Time Formatter adds a new display style to Emfield video cck module.

Currently the Emfield video_cck contrib module only fetches YouTube thumbnail. YouTube
Thumbnail With Time Formatter goes one-level up by leveraging the power of the Zend Framework 
module which has GData API bundled in.

To have the same functionality as seen in the module demo, simply;

- Make sure you have a content type setup for this.
- Navigate to admin/content/types/video/display
- Under teaser, Choose "YouTube Thumbnail With Time"
- Hit "Save".

Note: Make sure Zend Framework module is configured properly and it's status report is marked "green".



